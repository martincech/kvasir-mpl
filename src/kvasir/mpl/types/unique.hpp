#pragma once
#include "template.hpp"
#include "list.hpp"
#include "bool.hpp"

namespace kvasir {
	namespace mpl {
      namespace detail{
         template<bool FirstTwoSame, typename T, template<typename, typename > class Pred, typename... Ts>
         struct Unique;
         template<typename... Rs, template<typename, typename > class Pred, typename T, typename U>	//only two left
         struct Unique<true, list<Rs...>, Pred, T, U> : list<Rs...,T>{};
         template<typename... Rs, template<typename, typename > class Pred, typename T, typename U>	//only two left
         struct Unique<false, list<Rs...>, Pred, T, U> : list<Rs...,T,U>{};
         template<typename... Rs, template<typename, typename > class Pred, typename T, typename U, typename V, typename... Ts>  //not same
         struct Unique<false,list<Rs...>, Pred, T, U, V, Ts...> : Unique<Pred<U,V>::value,list<Rs...,T>,Pred,U,V,Ts...>{};
         template<typename... Rs, template<typename, typename > class Pred, typename T, typename U, typename V, typename... Ts>  //same
         struct Unique<true,list<Rs...>, Pred, T, U, V, Ts...> : Unique<Pred<U,V>::value,list<Rs...>,Pred,U,V,Ts...>{};

      }
	//expercts a sorted list, removes all consecutive duplicates fullfilling pred
	template<typename TList, typename TPred = false_>
	struct Unique{
		static_assert(eager::always_false<TList>::value,"implausible parameters");
	};

	template< template<typename, typename > class TPred>
	struct Unique<list<>, Template<TPred>> {
		using type = list<>;
	};
	template<typename T, template<typename, typename > class TPred>
	struct Unique<list<T>, Template<TPred>> {
		using type = list<T>;
	};
	template<typename T, typename U, typename...Ts, template<typename, typename > class TPred>
	struct Unique<list<T,U,Ts...>, Template<TPred>> : detail::Unique<TPred<T,U>::value, list<>, TPred, T,U,Ts...>{};

	template<typename TList, typename TPred = false_>
	using UniqueT = typename Unique<TList, TPred>::type;
   }
}