#pragma once

namespace kvasir {
	namespace mpl {
      //wrapper for template template parameters
		template<template<typename...> class T>
		struct Template{
			template<typename... Ts>
			using apply = T<Ts...>;
		};

		template<typename T, typename... Ts>
		using ApplyTemplateT = typename T::template apply<Ts...>::type;
   }
}